package org.codingdojo.kata.tennis;

import java.util.Objects;

public class Score {
    private int p1 = 0;
    private int p2 = 0;

    public Score(int p1, int p2) {
        this.p1 = p1;
        this.p2 = p2;
    }

    public Score() {

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Score score = (Score) o;
        return p1 == score.p1 && p2 == score.p2;
    }

    @Override
    public int hashCode() {
        return Objects.hash(p1, p2);
    }

    public void player1Win() {
        p1 = 15;
    }

    @Override
    public String toString() {
        return p1 + " - " + p2;
    }
}
