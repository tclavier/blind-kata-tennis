package org.codingdojo.kata.tennis;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TennisTest {
    Score score = new Score();

    @Test
    void starting_score_should_be_zero() {
        Score expected = new Score(0,0);
        Assertions.assertEquals(expected, score());
    }

    @Test
    void after_first_point_score_should_be_15_0() {
        Score expected = new Score(15,0);
        player1Win();
        Assertions.assertEquals(expected, score());
    }

    private void player1Win() {
        score.player1Win();
    }

    private Score score() {
        return score;
    }
}
